#include "include/gameScreen.h"
#include "include/menuScreen.h"
#include "include/overScreen.h"
#include "include/soundEffects.h"
#include "include/musicManager.h"
#include "include/pad.h"

extern StateMachine GameMachineState;
extern Controller PlaystationGamePad;

TitleScreen titly;
MenuText texty;

SE StartGameSE;
BGM MenuMusic;

extern char* root;

float moveSpeed = 2.0f;

void MenuStart(GSGLOBAL* gsGlobal)
{
	char* tempRoot = "";
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_bmp(gsGlobal, &titly.texture, strcat(tempRoot,"Graphics/Images/Title.bmp"));
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_bmp(gsGlobal, &texty.texture, strcat(tempRoot,"Graphics/Images/Texty.bmp"));
	titly.posX = 0.0f;
	titly.posY = 0.0f;
	texty.posX = 100.0f;
	texty.posY = 100.0f;
	initFormat();
	initMusicFormat();
	
	strncpy(tempRoot, root, strlen(root));
	StartGameSE.fileName = strcat(tempRoot,"Audio/SE/Confirm.adp");
	LoadSoundEffect(&StartGameSE);
	
	strncpy(tempRoot, root, strlen(root));
	MenuMusic.fileName = strcat(tempRoot,"Audio/BGM/TitleScreenResample.wav");
	LoadMusic(&MenuMusic);
}

void MenuUpdate(GSGLOBAL* gsGlobal)
{
	// This part here plays the music
	PlayMusic(&MenuMusic);
	
	
	if(texty.posX > 250.0f)
	{
		moveSpeed = -2.0f;
	}
	
	if(texty.posX < 50.0f)
	{	
		moveSpeed = 2.0f;
	}
	
	texty.posX += moveSpeed;
	
	if(PlaystationGamePad.START_KEY_TAP)
	{
		PlaySoundEffect(&StartGameSE, 0);
		StateMachineChange(&GameMachineState, &GameState, gsGlobal);
	}
}

void MenuDraw(GSGLOBAL* gsGlobal, u64 colour)
{

	gsKit_prim_sprite_texture(gsGlobal, &titly.texture,	titly.posX,  // X1
						titly.posY,  // Y2
						0.0f,  // U1
						0.0f,  // V1
						titly.texture.Width + titly.posX, // X2
						titly.texture.Height +  titly.posY, // Y2
						titly.texture.Width, // U2
						titly.texture.Height, // V2
						2,
						colour);
	gsKit_prim_sprite_texture(gsGlobal, &texty.texture,	texty.posX,  // X1
						texty.posY,  // Y2
						0.0f,  // U1
						0.0f,  // V1
						texty.texture.Width + texty.posX, // X2
						texty.texture.Height +  texty.posY, // Y2
						texty.texture.Width, // U2
						texty.texture.Height, // V2
						2,
						colour);
}

void MenuEnd(GSGLOBAL* gsGlobal)
{
	printf("This should Run when MenuState is Gone.\n");
	// Clear VRAM after the Menu is done~
	gsKit_vram_clear(gsGlobal);
	// End Music
	UnloadSoundEffect(&StartGameSE);
	UnloadMusic(&MenuMusic);
}

StateManager MenuState =
{
	MenuStart,
	MenuUpdate,
	MenuDraw,
	MenuEnd
};
