#include "include/starfield.h"
#include <stdlib.h>

Starfield stars;
 
 
extern char* root;


void InitializeStarfield(GSGLOBAL* gsGlobal)
{
	char* tempRoot = "";
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_bmp(gsGlobal, &stars.texture, strcat(tempRoot,"Graphics/Background/SF.bmp"));
	
	stars.posX = 0.0f;
	stars.posY = 0.0f;
	stars.pos2X = 0.0f;
	stars.pos2Y = -512.0f;
	stars.TexCol = GS_SETREG_RGBAQ(0x80,0x80,0x80,0x80,0x00);

}

void ScrollBackground(GSGLOBAL *gsGlobal)
{
	stars.posY+= 4.0f;	
	stars.pos2Y+= 4.0f;	
	//printf("Y1 = %.2f Y2 = %.2f\n", stars.posY, stars.pos2Y);	
	// If Stars image reaches end of screen.	
	if(stars.posY > 512.0f)
	{
		stars.posY = -512.0f;
	}
	
	if(stars.pos2Y > 512.0f)
	{
		stars.pos2Y = -512.0f;
	}
}

void DrawBackground(GSGLOBAL *gsGlobal, u64 colour)
{
	gsKit_prim_sprite_texture(gsGlobal, &stars.texture,	stars.posX,  // X1
						stars.posY,  // Y2
						0.0f,  // U1
						0.0f,  // V1
						stars.texture.Width + stars.posX, // X2
						stars.texture.Height + stars.posY, // Y2
						stars.texture.Width, // U2
						stars.texture.Height, // V2
						2,
						stars.TexCol);
						
	gsKit_prim_sprite_texture(gsGlobal, &stars.texture,	stars.pos2X,  // X1
						stars.pos2Y,  // Y2
						0.0f,  // U1
						0.0f,  // V1
						stars.texture.Width + stars.pos2X, // X2
						stars.texture.Height + stars.pos2Y, // Y2
						stars.texture.Width, // U2
						stars.texture.Height, // V2
						2,
						stars.TexCol);
}
