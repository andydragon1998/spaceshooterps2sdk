#include "include/gameScreen.h"
#include "include/menuScreen.h"
#include "include/overScreen.h"
#include "include/musicManager.h"
#include "include/pad.h"

extern StateMachine GameMachineState;
extern Controller PlaystationGamePad;

OverScreen overy;

BGM GameOverMusic;

u64 OverScreenColour;

extern char* root;


void OverStart(GSGLOBAL* gsGlobal)
{
	char* tempRoot = "";
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_bmp(gsGlobal, &overy.texture, strcat(tempRoot,"Graphics/Images/GameOver.bmp"));

	OverScreenColour = GS_SETREG_RGBAQ(0x80,0x80,0x80,0x80,0x00);

	overy.posX = 0.0f;
	overy.posY = 0.0f;

	initFormat();
	initMusicFormat();
	
	strncpy(tempRoot, root, strlen(root));
	GameOverMusic.fileName = strcat(tempRoot,"Audio/BGM/GameOverMono.wav");
	LoadMusic(&GameOverMusic);
}

void OverUpdate(GSGLOBAL* gsGlobal)
{
	PlayMusic(&GameOverMusic);
	
	if(PlaystationGamePad.START_KEY_TAP)
	{

		StateMachineChange(&GameMachineState, &MenuState, gsGlobal);
	}
}

void OverDraw(GSGLOBAL* gsGlobal, u64 colour)
{
	gsKit_prim_sprite_texture(gsGlobal, &overy.texture,	overy.posX,  // X1
						overy.posY,  // Y2
						0.0f,  // U1
						0.0f,  // V1
						overy.texture.Width + overy.posX, // X2
						overy.texture.Height +  overy.posY, // Y2
						overy.texture.Width, // U2
						overy.texture.Height, // V2
						2,
						OverScreenColour);
}

void OverEnd(GSGLOBAL* gsGlobal)
{
	// Clear VRAM after the Menu is done~
	gsKit_vram_clear(gsGlobal);
	// End Music
	UnloadMusic(&GameOverMusic);
}

StateManager OverState =
{
	OverStart,
	OverUpdate,
	OverDraw,
	OverEnd
};
