#ifndef ENTITY_MANAGER
#define ENTITY_MANAGER


#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>
#include <stdbool.h>
#include "pad.h"

typedef struct
{
	int Health;
	int Heat;
	float X;
	float Y;
	float Speed;
	bool shoot;
	bool isAlive;

}Player;

typedef struct
{
	float X;
	float Y;
	float speed;
	bool toExplode;
	int ExplosionFrame;
	bool isAlive;
} Asteroid;

typedef struct
{
	float X;
	float Y;
	float speed;
	bool isAlive;
	
}PlayerBullet;



void InitializeEntities(GSGLOBAL* gsGlobal);
void UpdateInputs();
void UpdateEntities(GSGLOBAL* gsGlobal);
void DrawEntities(GSGLOBAL* gsGlobal, u64 colour);
void textureSetup(GSTEXTURE *texture);
void SpawnAsteroid(float posX, float posY);
void SpawnBullet(float posX, float posY);
void asteroidHitCheck();
void UpdateInput();
void UnloadEverything(GSGLOBAL* gsGlobal);


#endif
